package g56037.luckynumbers.view;

import g56037.luckynumbers.model.Position;

/**
 * Interface for the MyView view. Provides a display for the user.
 * 
 * @author Alan
 */
public interface View {
    /**
     * Gives information about lucky numbers game.
     * 
     * Information like the author, the version and the name of the game are 
     * given.
     * 
     */
    void displayWelcome();
    
    /**
     * Gives information about the current state of the game.
     * 
     * The current player, his board and his tile that he has to put are shown.
     * 
     */
    void displayGame();
    
    /**
     * Displays the number of the winner of the current game.
     * 
     */
    void displayWinner();
    
    /**
     * Asks how much players are participating to the current game. It provides
     * a number between 2 and 4.
     * 
     * @return the number of players 
     */
    int askPlayerCount();
    
    /**
     * Asks to the user a number of row and column and transforms 
     * it to a Position.
     * 
     * @throws IllegalArgumentException if the position is not correct
     * @return the position
     */
    Position askPosition();
    
    /**
     * Displays the error message given.
     * 
     * @param message the error message
     */
    void displayError(String message);
    
    /**
     * Asks to the user from which deck he wants to pick a tile. 
     * 
     * There is two deck, one with face down tiles and the other is with face up
     * tiles.
     * 
     * @return true if the user put y, false if the user put n
     */
    boolean askFaceUpOrDown();
    
    /**
     * Asks the user if the user wants to put the picked tile on his board or on
     * the table.
     * 
     * @return t if the user wants to put the tile on the table, b otherwise 
     */
    boolean whereToPut();
    
    /**
     * Asks with a robust form which tile user wants to pick.
     * 
     * If the user wants to pick a face up tiles it asks the position of the 
     * tile that the user wants to pick. If the user gives a wrong index, the 
     * method asks to try again.
     * 
     * @param size the size of the list
     * @return the index given by the user
     */
    int askIndex(int size);
    
    /**
     * Asks if the player wants to play again.
     * 
     * At the end of the game, this method asks to the player if he wants to 
     * play again or not.
     * 
     * @return y if the player wants to play again n otherwise
     */
    boolean playAgain();
    
    /**
     * Displays the first four tiles.
     * 
     */
    void displayFourTiles();
    
    public boolean endAsk();
}
