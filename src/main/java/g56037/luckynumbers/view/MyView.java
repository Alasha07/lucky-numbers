package g56037.luckynumbers.view;

import g56037.luckynumbers.model.Model;
import g56037.luckynumbers.model.Position;
import g56037.luckynumbers.model.Tile;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Implements the interface View.
 *
 * @author Alan
 */
public class MyView implements View {

    private final Model model;
    private final int maxPlayers = 4;
    private final int minPlayers = 2;
    
    /**
     * Creates an object myview.
     * 
     * @param model the object game
     */
    public MyView(Model model) {
        this.model = Objects.requireNonNull(model, "Model not found");
    }

    @Override
    public void displayWelcome() {
        System.out.println("* * * * * * * * * * * * * * * * * * * * *");
        System.out.println("* Welcome to Lucky Numbers !            *");
        System.out.println("* -> Author : Alan                      *");
        System.out.println("* -> Version : 2.0 console.v            *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * *");
    }

    @Override
    public void displayGame() {
        // +1 to facilitate the user : player 0 shown as player 1.
        System.out.println("-----------------------");
        System.out.println("| The picked tile : " + model.getPickedTile()
                .getValue() + " |");
        System.out.println("-----------------------");
        displayBoardPlayer();
    }
    
    /**
     * Displays the board of a player.
     * 
     * Method destinated to be used in display game.
     * 
     */
    private void displayBoardPlayer() {
        gameHeading();
        for (int i = 0; i < model.getBoardSize(); i++) {
            System.out.print((i + 1) + "|");
            for (int j = 0; j < model.getBoardSize(); j++) {
                Tile tile = model.getTile(model.getCurrentPlayerNumber(),
                        new Position(i, j));
                if (tile != null) {
                    String theTile = String.format("%3d", tile.getValue());
                    System.out.print(theTile);
                } else {
                    String nullCase = String.format("%3c", '.',"\\s");
                    System.out.print(nullCase);
                }
            }
            System.out.println();
        }
        gameFooter();
    }
    
    /**
     * Displays the heading of the game. According to the number of column of 
     * the array.
     * 
     */
    private void gameHeading(){
        System.out.printf("%2s"," ");
        for (int i = 0 ; i < model.getBoardSize() ; i++){
            String index = String.format("%3d", (i+1));
            System.out.print(index);
        }
        System.out.println();
        
        System.out.printf("%2s"," ");
        for(int i = 0 ; i < model.getBoardSize() ; i++){
            String line = String.format("%3c", '-');
            System.out.print(line);
        }
        System.out.println();
    }
    
    /**
     * Displays the footer of the game. According to the number of column of 
     * the array.
     * 
     */
    private void gameFooter(){
        System.out.printf("%2s"," ");
        for(int i = 0 ; i < model.getBoardSize() ; i++){
            String line = String.format("%3c", '-');
            System.out.print(line);
        }
        System.out.println();
    }

    @Override
    public void displayWinner() {
        List <Integer> winners = model.getWinners();
        for(int i = 0 ; i < winners.size() ; i++){
            winners.set(i, winners.get(i) + 1);
        }
        System.out.println("*******************************");
        System.out.println("* The winner(s) of the game : " + winners + "*");
        System.out.println("*******************************");
    }

    @Override
    public int askPlayerCount() {
        int nbPlayers = readInteger(
                            "How manny players do you want to play with ?");
        while(nbPlayers < minPlayers || nbPlayers > maxPlayers){
            displayError("The number of players is not valid. Try again.");
            nbPlayers = readInteger(
                    "How manny players do you want to play with ?");
        }
        return nbPlayers;
    }

    @Override
    public Position askPosition() {
        int row = readInteger("Enter a row (integer number) : ") - 1;
        int column = readInteger("Enter a column (integer number) : ") - 1;
        
        while(!model.isInside(new Position (row,column)) || 
                !model.canTileBePut(new Position (row,column))){
            displayError("You entered an incorrect position. Try again ");
            row = readInteger("Enter a row (integer number) : ") - 1;
            column = readInteger("Enter a column (integer number) : ") - 1;
        }
        
        Position pos = new Position(row, column);

        return pos;
    }

    /**
     * Asks with the robust form to a user to enter an integer number.
     *
     * @param message the message to pass to the user
     * @return an integer entered by the user
     */
    private int readInteger(String message) {
        Scanner kbd = new Scanner(System.in);
        System.out.println(message);
        while (!kbd.hasNextInt()) {
            kbd.next();
            System.out.println("The number entered is not an integer.");
            System.out.println(message);
        }
        return kbd.nextInt();
    }

    @Override
    public void displayError(String message) {
        System.err.println(message);
    }
    
    @Override
    public boolean askFaceUpOrDown(){
        System.out.println("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨");
        System.out.println("¨¨PLAYER NUMBER " + 
                            (model.getCurrentPlayerNumber() + 1 ) + " TURN ¨¨");
        System.out.println("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨");
        
        System.out.println("x " + model.faceDownTileCount());
        displayFaceUpTiles();
        
        boolean isFaceDown = false;
        String answer = readString("Do you want a face down tile ? (y or n)");
        while(!answer.equalsIgnoreCase("y") && !answer.equalsIgnoreCase("n")){
            System.out.println("You must type y for yes or n for no.");
            answer = readString("Try again.");
        }
        isFaceDown = answer.equalsIgnoreCase("y");
        
        return isFaceDown;
    }
    
    @Override
    public int askIndex(int size){
        int index = (readInteger("Which tile do you want to pick ?") - 1);
        int listBegining = 0;
        
        while(index >= size || index < listBegining){
            System.out.println("You must put a number from 1 to "+size);
            index = (readInteger("Try again") - 1);
        }
        int returnedIndex = index;
        return returnedIndex;
    }
    
    /**
     * Displays face up tiles.
     * 
     */
    private void displayFaceUpTiles(){
        for (Tile tile : model.getAllFaceUpTiles()) {
            System.out.print(tile.getValue() + " ");
        }
        System.out.println();
    }
    
    /**
     * Asks with the robust form to a user to enter a String.
     * 
     * @param message the message to pass to the user
     * @return a string entered by the user
     */
    private static String readString(String message) {
        Scanner kbd = new Scanner(System.in);
        System.out.println(message);
        while (!kbd.hasNextLine()) {
            kbd.next();
            System.out.println("This is not a String.");
            System.out.println(message);
        }
        return kbd.nextLine();
    }
    
    @Override
    public boolean whereToPut(){
        String answer = readString("If you want to put the tile on your board, put"
                + " (b). If you want to put it on the table put (t)");
        boolean isOnBoard = false;
        
        while(!answer.equalsIgnoreCase("b") && 
                !answer.equalsIgnoreCase("t")){
            System.out.println("You must type b or t.");
            answer = readString("Try again.");
        }
        isOnBoard = answer.equalsIgnoreCase("b");
        
        return isOnBoard;
    }
    
    @Override
    public boolean playAgain(){
        String answer = readString("Do you want to play again ? (y) (n)");
        boolean isApproved = false;
        
        while(!answer.equalsIgnoreCase("y") && !answer.equalsIgnoreCase("n")){
            displayError("You must put y or n.");
            answer = readString("Try again :");
        }
        isApproved = answer.equalsIgnoreCase("y");
        
        return isApproved;
    }
    
    @Override
    public void displayFourTiles(){
        System.out.println("Player : "+(model.getCurrentPlayerNumber()+1));
        System.out.println("- - - - - - - ");
        for(Tile tile : model.getListFourTiles()){
            System.out.print(tile.getValue() + " ");
        }
        System.out.println("\n- - - - - - - ");
        System.out.println("Put the tile at a diagonal position.");
    }
    
    @Override
    public boolean endAsk(){
        String answer = readString("Do you want to remove the face up tiles ?");
        while(!answer.equalsIgnoreCase("y") && !answer.equalsIgnoreCase("n")){
            displayError("Put y (yes) or n (no), try again.");
            answer = readString("Do you want to remove the face up tiles ?");
        }
        return answer.equalsIgnoreCase("y");
    }
}
