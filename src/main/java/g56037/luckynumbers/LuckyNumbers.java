package g56037.luckynumbers;

import g56037.luckynumbers.controller.Controller;
import g56037.luckynumbers.model.Game;
import g56037.luckynumbers.model.Model;
import g56037.luckynumbers.view.MyView;

/**
 * Plays a game of Lucky Numbers.
 * 
 * By classes Controller, Game and MyView this class allow two to four users to
 * play to lucky numbers only via the console. It is not the official game. At 
 * the diagonal there is no tiles that are spawned at the begin of the game. To
 * learn the rules go to Board Game Arena. This class contains only a main 
 * method.
 * 
 * @author Alan
 */
public class LuckyNumbers {
    public static void main(String[] args) {
        Model game = new Game();
        Controller controller = new Controller(game, new MyView(game));
        controller.play();
    }
}
