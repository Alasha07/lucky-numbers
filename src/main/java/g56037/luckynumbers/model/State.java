package g56037.luckynumbers.model;

/**
 * Gives the state of the game at a specific moment.
 * 
 * If the game has not started the current state is NOT_STARTED, if it has there
 * are two states PICK_TILE or PLACE_TILE. Finally the game is over or it turns
 * to the end with these values : GAME_OVER, TURN_END respectively.
 * 
 * @author Alan
 */
public enum State {
    /**
     * When the game has not started the state is not started.
     */
    NOT_STARTED,
    /**
     * Once the game has started the state turns to pick tile.
     */
    PICK_TILE,
    /**
     * When the player has picked a tile the state turns to palce tile.
     */
    PLACE_TILE,
    /**
     * Once the player has putted his tile on the board the state becomes turn
     * end.
     */
    TURN_END,
    /**
     * Finally if the last case of the board was putted, the state turns to game
     * over.
     */
    GAME_OVER,
    /**
     * Turns to place or drop tile state. If the player decide to drop it, it
     * will be placed to the visible deck otherwise it places the tile.
     * 
     */
    PLACE_OR_DROP_TILE
}
