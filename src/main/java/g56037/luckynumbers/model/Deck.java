package g56037.luckynumbers.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class manipulates two different decks. The non visible deck and the 
 * visible deck where tiles are visible.
 * 
 * @author Alan
 */
public class Deck {
    
    private final List <Tile> faceUpTiles;
    private final List <Tile> faceDownTiles;
    
    /**
     * Creates a Deck object.
     * 
     * @param playerCount the number of players
     */
    public Deck(int playerCount){
        this.faceDownTiles = new ArrayList <>();
        this.faceUpTiles = new ArrayList <>();
        int tileLimit = 20;
        for(int i = 0 ; i < playerCount ; i++){
            for(int j = 1; j <= tileLimit ; j++){
                faceDownTiles.add(new Tile(j));
            }
        }
    }
    
    /**
     * Gives the number of no visible tiles.
     * 
     * @return a number
     */
    public int faceDownCount(){
        return faceDownTiles.size();
    }
    
    /**
     * Picks a random tile from the face down deck.
     * 
     * @return a random Tile
     */
    public Tile pickFaceDown(){
        Collections.shuffle(faceDownTiles);
        Tile pickedTile = faceDownTiles.get(0);
        faceDownTiles.remove(0);
        pickedTile.flipFaceUp();
        return pickedTile;
    }
    
    /**
     * Picks the tile indicated by the index. 
     * 
     * It is written to be tested instead of pickFaceDown because of it random
     * behavior.
     * 
     * @param index the indicator
     * @return a tile
     */
    protected Tile pickFaceDown(int index){
        Tile pickedTile = faceDownTiles.get(index);
        faceDownTiles.remove(index);
        pickedTile.flipFaceUp();
        return pickedTile;
    }
    
    /**
     * Gives the number of visible tiles.
     * 
     * @return an integer number
     */
    public int faceUpCount(){
        return faceUpTiles.size();
    }
    
    /**
     * Gets the list of visible tiles.
     * 
     * @return return the list of visible tiles
     */
    public List<Tile> getAllFaceUp(){
        return faceUpTiles;
    }
    
    /**
     * Checks if the given tile exists and if it is visible in the deck.
     * 
     * @param tile the tile to be checked
     * @throws IllegalArgumentException if the given tile is not between 1 and 
     * 20 included
     * @return true if the tile exists and if it is visible on the deck, false 
     * otherwise
     */
    public boolean hasFaceUp(Tile tile){
        if(tile == null){
            throw new NullPointerException("The tile is null.");
        }
        if(tile.getValue() < 1 || tile.getValue() > 20){
            throw new IllegalArgumentException("The tile is not valid ( 1 to 20"
                    + " include).");
        }
        boolean isFound = false;
        int i = 0;
        while( i < faceUpTiles.size() && !isFound){
            isFound = faceUpTiles.get(i).getValue() == tile.getValue();
            i++;
        }
        return isFound;
    }
    
    /**
     * Removes from the face up deck the tile given.
     * 
     * @throws IllegalArgumentException if the tile does not appear in the 
     * visible deck
     * @param tile the tile to be removed
     */
    public void pickFaceUp(Tile tile){
        if(faceUpTiles.isEmpty() || !hasFaceUp(tile)){
            throw new IllegalArgumentException("The tile doesn't exist.");
        }
        int removeIndex = faceUpTiles.indexOf(tile);
        faceUpTiles.remove(removeIndex);
    }
    
    /**
     * Puts back in the visible deck the given tile.
     * 
     * @param tile the tile to be put back
     */
    public void putBack(Tile tile){
        faceUpTiles.add(tile);
    }
    
    public void putBackDown(Tile tile){
        faceDownTiles.add(tile);
    }
    
    /**
     * Gets the face down tiles.
     * 
     * @return return the list of the down tiles
     */
    List <Tile> getFaceDownTiles (){
        return faceDownTiles;
    }
}
