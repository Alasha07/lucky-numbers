package g56037.luckynumbers.model;

/**
 * Implements a board for the game Lucky Numbers
 * 
 * It creates a board of 4x4 and gives differents methods to check for example if
 * a board is full or not.
 * 
 * @author Alan
 */
public class Board {

    private final Tile[][] tiles;
    private final int SIZE = 4;

    /**
     * Creates a Tile array of 4x4.
     *
     */
    public Board() {
        tiles = new Tile[SIZE][SIZE];
    }
    
    /**
     * Gets the size of the Tile array.
     *
     * @return the size
     */
    public int getSize() {
        return tiles.length;
    }

    /**
     * Checks if the given position is a correct position in the array.
     *
     * @param pos the given position
     * @return true if the position is correct false otherwise
     */
    public boolean isInside(Position pos) {
        return (pos.getColumn() >= 0 && pos.getColumn() <= getSize() - 1)
                && pos.getRow() >= 0 && pos.getRow() <= getSize() - 1;
    }

    /**
     * Gets the Tile at the specified position.
     *
     * @param pos the given position
     * @return the tile if the position is initialized null otherwise
     */
    public Tile getTile(Position pos) {
        return tiles [pos.getRow()][pos.getColumn()];
    }

    /**
     * Checks if the tile can be putted at the specified position on the board.
     *
     * @param tile the String that represents the tile
     * @param pos the position where the tile could be putted
     * @return true if the position is correct false otherwise
     */
    public boolean canBePut(Tile tile, Position pos) {
        return checkLeft(tile, pos) && checkRight(tile, pos) && 
                checkTop(tile, pos) && checkBottom(tile, pos);
    }
    
    /**
     * Checks the left side of the position.
     * 
     * @param tile the String that represents the tile
     * @param pos the position where the tile could be putted
     * @return true if the position is correct false otherwise
     */
    private boolean checkLeft(Tile tile, Position pos){
        int ligne  = pos.getRow();
        for(int colonne = 0 ; colonne < pos.getColumn() ; colonne++){
            if(tiles[ligne][colonne] != null && 
                    tiles[ligne][colonne].getValue() >= tile.getValue()){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Checks the right side of the position.
     * 
     * @param tile the String that represents the tile
     * @param pos the position where the tile could be putted
     * @return true if the position is correct false otherwise
     */
    private boolean checkRight(Tile tile, Position pos){
        int ligne  = pos.getRow();
        for(int colonne = getSize() - 1 ; colonne > pos.getColumn() ; colonne--){
            if(tiles[ligne][colonne] != null &&
                    tiles[ligne][colonne].getValue() <= tile.getValue()){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Checks the top side of the position.
     * 
     * @param tile the String that represents the tile
     * @param pos the position where the tile could be putted
     * @return true if the position is correct false otherwise 
     */
    private boolean checkTop(Tile tile, Position pos){
        int colonne = pos.getColumn();
        for(int lg  = 0 ; lg < pos.getRow() ; lg++){
            if(tiles[lg][colonne] != null &&
                    tiles[lg][colonne].getValue() >= tile.getValue()){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Checks the bottom side of the position.
     * 
     * @param tile the String that represents the tile
     * @param pos the position where the tile could be putted
     * @return true if the position is correct false otherwise
     */
    private boolean checkBottom(Tile tile, Position pos){
        int colonne = pos.getColumn();
        for(int lg = getSize() - 1 ; lg > pos.getRow(); lg--){
            if(tiles[lg][colonne] != null &&
                    tiles[lg][colonne].getValue() <= tile.getValue()){
                return false;
            }
        }
        return true;
    }

    /**
     * Puts the tile at the specified position on the board.
     *
     * @param tile the tile to put on the board
     * @param pos the position where the tile is putted
     */
    public void put(Tile tile, Position pos) {
        int row = pos.getRow();
        int column = pos.getColumn();
        tiles[row][column] = tile;
    }

    /**
     * Checks if the board is completely filled by tiles.
     *
     * @return true if the board is completely filled false otherwise
     */
    public boolean isFull() {
        int i = 0;
        while(i < getSize()){
            int j = 0;
            while(j < getSize()){
                if(tiles[i][j] == null){
                    return false;
                }
                j++;
            }
            i++;
        }
        return true;
    }
    
    /**
     * Computes the number of non empty boxes on a 2D array of a player.
     * 
     * @return the number of non empty cases
     */
    protected int nonEmptyBoxes(){
        Tile [][] playerTiles = tiles;
        int cpt = 0;
        for(int row = 0 ; row < getSize() ; row++){
            for(int column = 0 ; column < getSize() ; column++){
                if(playerTiles[row][column] != null){
                    cpt++;
                }
            }
        }
        return cpt;
    }
}
