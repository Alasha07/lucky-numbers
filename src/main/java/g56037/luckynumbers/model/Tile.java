package g56037.luckynumbers.model;

/**
 * Creates the tiles of the game.
 * 
 * @author Alan
 */
public class Tile {
    
    private final int value;
    private boolean faceUp = false;
    
    /**
     * Instantiates the Tile object.
     * 
     * @param value the integer that represents the value
     */
    public Tile (int value){
        this.value = value;
    }
    
    /**
     * Gets the private attribute value.
     * 
     * @return the value
     */
    public int getValue() {
        return value;
    }
    
    /**
     * Gets the private attribute faceUp which shows if a tile is hidden or not.
     * 
     * @return false if the Tile is not visible true otherwise
     */
    public boolean isFaceUp(){
        return faceUp;
    }
    
    /**
     * Flips a tile to not hidden if it is hidden. Otherwise does nothing.
     * 
     */
    void flipFaceUp(){
        if(!this.faceUp){
            this.faceUp = true;
        }
    }
}
