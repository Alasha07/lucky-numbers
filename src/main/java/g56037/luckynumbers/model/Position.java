package g56037.luckynumbers.model;

/**
 * Represents a position on the game board.
 * 
 * Instead of using the line and the column to represent a position, the method 
 * uses the both parameters and makes a position.
 * 
 * @author Alan
 */
public class Position {
    
    private final int row;
    private final int column;
    
    /**
     * Instantiates the Position object.
     * 
     * @param row the integer represeting the row
     * @param column the integer represeting the column
     */
    public Position(int row, int column){
        this.column = column;
        this.row = row;
    }
    
    /**
     * Gets the private attribute row.
     * 
     * @return the row
     */
    public int getRow() {
        return row;
    }
    
    /**
     * Gets the private attribute column.
     * 
     * @return the column
     */
    public int getColumn() {
        return column;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.row;
        hash = 53 * hash + this.column;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.row != other.row) {
            return false;
        }
        if (this.column != other.column) {
            return false;
        }
        return true;
    }
    
    
}
