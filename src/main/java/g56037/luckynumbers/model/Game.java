package g56037.luckynumbers.model;

import static g56037.luckynumbers.model.State.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implements the interface Model.
 * 
 * It gathers the different elements of the game to implements the differents
 * steps of the game.
 * 
 * @author Alan
 */
public class Game implements Model {
    
    private State state;
    private int playerCount;
    private int currentPlayerNumber;
    private Board [] board;
    private Tile pickedTile;
    private Deck deck;
    private final int fourTiles = 4;
    private List <Tile> listFourTiles;
    private List <Position> diagonalPositions;
    
    /**
     * Creates an object game and turns the state to not started.
     * 
     */
    public Game(){
        this.state = NOT_STARTED;
    }
    
    @Override
    public void start(int playerCount){
        if(this.state != NOT_STARTED && this.state != GAME_OVER){
            throw new IllegalStateException("The state must be NOT_STARTED or"
                    + " GAME_OVER.");
        }
        if(playerCount < 2 || playerCount > 4){
            throw new IllegalArgumentException("The number of players muste be"
                    + " at list 2 and 4 maximum.");
        }
        this.playerCount = playerCount;
        this.diagonalPositions = new ArrayList <>();
        this.board = new Board [this.playerCount];
        for(int i = 0 ; i < board.length ; i++){
            board [i] = new Board();
        }
        this.deck = new Deck(playerCount);
        this.listFourTiles = new ArrayList <>();
        this.currentPlayerNumber = 0;
        this.state = PICK_TILE;
    }
    
    @Override
    public int getBoardSize(){
        return board[0].getSize();
    }
    
    @Override
    public Tile pickFaceDownTile(){
        if(this.state != PICK_TILE){
            throw new IllegalStateException("The state is not PICK_TILE.");
        }
        this.pickedTile = deck.pickFaceDown();
        this.state = PLACE_OR_DROP_TILE;
        
        return pickedTile;
    }
    
    /**
     * Pick a tile with the given value. Should be used only for the JUnit tests.
     * It is equivalent to pick a face down tile.
     * 
     * @param index the integer that gives the value of the tile
     * @return a Tile
     */
    protected Tile pickFaceDownTile(int index){
        if(this.state != PICK_TILE){
            throw new IllegalStateException("The state is not PICK_TILE.");
        }
        this.state = PLACE_OR_DROP_TILE;
        return this.pickedTile = deck.pickFaceDown(index);
    }
    
    /**
     * Pick a tile with the given value. Should be used only for several test.
     * 
     * @param value the intaiger that gives the value of the tile
     * @return a Tile
     */
    protected Tile pickTile(int value){
        this.state = PLACE_TILE;
        return this.pickedTile = new Tile(value);
    }

    
    @Override
    public void pickFaceUpTile(Tile tile){
        if(this.state != PICK_TILE){
            throw new IllegalStateException("The state is not PICK_TILE.");
        }
        this.state = PLACE_TILE;
        deck.pickFaceUp(tile);
        this.pickedTile = tile;
    }
    
    @Override
    public void dropTile(){
        if(this.state != PLACE_OR_DROP_TILE){
            throw new IllegalStateException("The state must be "
                    + "PLACE_OR_DROP_TILE.");
        }
        deck.putBack(this.pickedTile);
        
        List <Tile> faceDownTiles = deck.getFaceDownTiles();
        if(faceDownTiles.isEmpty()){
            this.state = GAME_OVER;
        }else{
            this.state = TURN_END;
        }
    }
    
    @Override
    public int faceDownTileCount(){
        if(this.state == NOT_STARTED){
            throw new IllegalStateException("The state is NOT_STARTED.");
        }
        return deck.faceDownCount();
    }
    
    @Override
    public int faceUpTileCount(){
        if(this.state == NOT_STARTED){
            throw new IllegalStateException("The state is NOT_STARTED.");
        }
        return deck.faceUpCount();
    }
    
    @Override
    public List <Tile> getAllFaceUpTiles(){
        if(this.state == NOT_STARTED){
            throw new IllegalStateException("The state is NOT_STARTED.");
        }
        return Collections.unmodifiableList(deck.getAllFaceUp());
    }
    
    @Override
    public void putTile(Position pos){
        if(this.state != PLACE_TILE && this.state != PLACE_OR_DROP_TILE){
            throw new IllegalStateException("The state must be PLACE_TILE or "
                    + "PLACE_OR_DROPTILE.");
        }
        if(!isInside(pos) || !canTileBePut(pos)){
            throw new IllegalArgumentException("Not allowed to put the tile at"
                    + " this position : "+pos.getRow()+", "+pos.getColumn());
        }
        
        if(board[this.currentPlayerNumber].getTile(pos) != null){
            Tile replaceTile = board[this.currentPlayerNumber].getTile(pos);
            replaceTile.flipFaceUp();
            deck.putBack(replaceTile);
        }
        board[this.currentPlayerNumber].put(pickedTile, pos);
        
        List <Tile> faceDownTiles = deck.getFaceDownTiles();
        if(board[this.currentPlayerNumber].isFull()){
            this.state = GAME_OVER;
        }else if (faceDownTiles.isEmpty()){
            this.state = GAME_OVER;
        }else if(!this.listFourTiles.isEmpty()){
            this.state = PLACE_TILE;
        }else{
            this.state = TURN_END;
        }
    }
    
    @Override
    public void nextPlayer(){
        if(this.state != TURN_END){
            throw new IllegalStateException("The state must be TURN_END.");
        }
        this.state = PICK_TILE;
        this.currentPlayerNumber = (currentPlayerNumber + 1) % playerCount;
    }
    
    @Override
    public int getPlayerCount(){
        if(this.state == NOT_STARTED){
            throw new IllegalStateException("The state is NOT_STARTED.");
        }
        return this.playerCount;
    }
    
    @Override
    public State getState(){
        return this.state;
    }
    
    @Override
    public int getCurrentPlayerNumber(){
        if(this.state == NOT_STARTED || this.state == GAME_OVER){
            throw new IllegalStateException("The state is NOT_STARTED or "
                    + "GAME_OVER.");
        }
        return this.currentPlayerNumber;
    }
    
    @Override
    public Tile getPickedTile(){
        if(this.state != PLACE_TILE && this.state != PLACE_OR_DROP_TILE){
            throw new IllegalStateException("The state must be PLACE_TILE.");
        }
        return this.pickedTile;
    }
    
    @Override
    public boolean isInside(Position pos){
        return board[this.currentPlayerNumber].isInside(pos);
    }
    
    @Override
    public boolean canTileBePut(Position pos){
        if(this.state != PLACE_TILE && this.state != PLACE_OR_DROP_TILE){
            throw new IllegalStateException("The state must be PLACE_TILE or"
                    + " PLACE_OR_DROP_TILE.");
        }
        if(!isInside(pos)){
            throw new IllegalArgumentException("The position must be inside the"
                    + " board.");
        }
        
        return board[this.currentPlayerNumber].canBePut(pickedTile, pos);
    }
    
    @Override
    public Tile getTile(int playerNumber, Position pos){
        if(this.state == NOT_STARTED){
            throw new IllegalStateException("The current state is not valid");
        }
        if(playerNumber < 0 || playerNumber > this.playerCount){
            throw new IllegalArgumentException("The number of the player is not"
                    + " valid.");
        }
        if(!isInside(pos)){
            throw new IllegalArgumentException("The position must be inside the"
                    + " board.");
        }
        return this.board[this.currentPlayerNumber].getTile(pos);
    }
    
    /**
     * Defines the winner(s) of the game.
     * 
     * @return a list of winners
     */
    private List <Integer> defineWinners(){
        List <Integer> winners = new ArrayList <>();
        List <Integer> boxes = new ArrayList <>();
        int max = 0;
        
        for (int i = 0 ; i < this.playerCount ; i++){
            int currentBoxes = board[i].nonEmptyBoxes();
            boxes.add(currentBoxes);
            if(max < currentBoxes){
                max = currentBoxes;
            }
        }
        
        for(int i = 0 ; i < this.playerCount ; i++){
            if(boxes.get(i) == max){
                winners.add(i);
            }
        }
        
        return winners;
    }
    
    @Override
    public List <Integer> getWinners(){
        if(this.state != GAME_OVER){
            throw new IllegalStateException("The state must be GAME_OVER");
        }
        List <Integer> winners = defineWinners();
        return winners;
    }
    
    @Override
    public void pickFourTiles(){
        for(int i = 0 ; i < this.fourTiles ; i++){
            listFourTiles.add(pickFaceDownTile());
            this.state = PICK_TILE;
        }
        this.state = PLACE_OR_DROP_TILE;
    }
    
    /**
     * Picks four tiles. Destinated to be used on the JUnit tests.
     * 
     * @param index the index of the tile to be picked
     */
    protected void pickFourTiles(int index){
        this.state = PICK_TILE;
        for(int i = 0 ; i < this.fourTiles ; i++){
            listFourTiles.add(pickFaceDownTile(index));
            this.state = PICK_TILE;
        }
        this.state = PLACE_OR_DROP_TILE;
    }
    
    @Override
    public void putDiagonal(int index, Position pos){
        if(index < 0 && index > listFourTiles.size()){
            throw new IllegalArgumentException("The index is not valid.");
        }
        this.pickedTile = listFourTiles.get(index);
        listFourTiles.remove(index);
        this.state = PLACE_OR_DROP_TILE;
        putTile(new Position(pos.getRow(),pos.getColumn()));
    }
    
    @Override
    public List <Tile> getListFourTiles(){
        if(this.state == NOT_STARTED){
            throw new IllegalStateException("The state is NOT_STARTED.");
        }
        return Collections.unmodifiableList(this.listFourTiles);
    }
    
    @Override
    public boolean checkDiagonalPosition(Position pos){
        boolean isAllowed = false;
        
        int index = 0;
        while(index < this.diagonalPositions.size() && !isAllowed){
            isAllowed = pos.equals(this.diagonalPositions.get(index));
            if(isAllowed && isEmptyPosition(pos)){
                this.diagonalPositions.remove(pos);
            }
            index++;
        }
        
        return isAllowed;
    }
    
    /**
     * Checks if the given position is empty or not.
     * 
     * @param pos the position to check
     * @return true if the position is empty false otherwise
     */
    private boolean isEmptyPosition(Position pos){
        return board[currentPlayerNumber].getTile(pos) == null;
    }
    
    @Override
    public void initializeDiagonalPositions(){
        int row = 0,column = 0;
        while(row < getBoardSize() && column < getBoardSize()){
            this.diagonalPositions.add(new Position(row,column));
            row++;
            column++;
        }
    }
    
    @Override
    public void putBackFaceUpTiles(){
        List <Tile> faceUp = deck.getAllFaceUp();
        int i = 0;
        while(!deck.getAllFaceUp().isEmpty()){
            Tile currentTile = faceUp.get(i);
            faceUp.remove(currentTile);
            deck.putBackDown(currentTile);
        }
        Collections.shuffle(deck.getFaceDownTiles());
    }
}
