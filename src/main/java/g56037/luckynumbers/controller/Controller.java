
package g56037.luckynumbers.controller;

import g56037.luckynumbers.model.Model;
import g56037.luckynumbers.model.Position;
import g56037.luckynumbers.model.Tile;
import g56037.luckynumbers.view.View;

/**
 * Controls the process of the Lucky Numbers game with the help of the interface
 * View and the interface Model.
 * 
 * @author Alan
 */
public class Controller {
    
    private final View view;
    private final Model model;
    
    /**
     * Creates the object Controller. 
     * 
     * @param model the object Game
     * @param view the object MyView
     */
    public Controller(Model model, View view){
        this.model = model;
        this.view = view;
    }
    
    /**
     * Plays a game of Lucky Numbers.
     * 
     * With the help of two other classes: MyView and Game, it controls the 
     * process of an entire game between the players. The game is controlled
     * through the differents states of the game.
     * 
     */
    public void play(){
        view.displayWelcome();
        boolean endGame = false;
        boolean isAgain;
        try{
            while(!endGame){
                quickPlay();
                isAgain = view.playAgain();
                if(!isAgain){
                    endGame = true;
                }else{
                    int nbPlayers = view.askPlayerCount();
                    model.start(nbPlayers);
                }
            }
        }catch(Exception e){
            view.displayError("An error was detected -> "
                    + "contact the author of the game. " + e.getMessage());
        }
    }
    
    /**
     * Plays a game. This method is written to be called in play.
     */
    private void quickPlay(){
        boolean remove = false;
        int noWinnerActually = -1;
        int listSize = -1;
        boolean isDiagonalFull = false;
        int cpt = 0;
        while(listSize == noWinnerActually){
            switch(model.getState()){
                case NOT_STARTED:
                    int nbPlayers = view.askPlayerCount();
                    model.start(nbPlayers);
                    break;
                case PICK_TILE:
                    while(!isDiagonalFull){
                        playDiagonal();
                        if(cpt == model.getPlayerCount() - 1){
                            isDiagonalFull = true;
                        }
                        model.nextPlayer();
                        cpt++;
                    }
                    pickATile();
                    break;
                case PLACE_TILE:
                    view.displayGame();
                    Position pos = view.askPosition();
                    model.putTile(pos);
                    break;
                case PLACE_OR_DROP_TILE:
                    view.displayGame();
                    boolean isOnBoard = view.whereToPut();
                    if(isOnBoard){
                        Position position = view.askPosition();
                        model.putTile(position);
                    }else{
                        model.dropTile();
                    }
                    break;
                case GAME_OVER:
                    listSize = model.getWinners().size();
                    view.displayWinner();
                    break;
                case TURN_END:
                    remove = view.endAsk();
                    if(remove){
                        model.putBackFaceUpTiles();
                    }
                    model.nextPlayer();
                    break;
            }
        }
    }
    
    /**
     * Picks a tile according to the answer of the player.
     * 
     * If the player wants a face up tiles he chooses the index of the tile that
     * he wants to pick. If there is not face up tiles, a face down tiles is 
     * picked. If the player cants a face down tiles, then a random one is 
     * chosen.
     * 
     */
    private void pickATile(){
        boolean isFaceDown = view.askFaceUpOrDown();
        if(!isFaceDown && !model.getAllFaceUpTiles().isEmpty()){
            int index = view.askIndex(model.getAllFaceUpTiles().size());
            Tile tile = model.getAllFaceUpTiles().get(index);
            model.pickFaceUpTile(tile);
        }else if (!isFaceDown && 
            model.getAllFaceUpTiles().isEmpty()){
            view.displayError("There is not face up tiles. A face "
                    + "down tile was picked.");
            model.pickFaceDownTile();
        }else{
            model.pickFaceDownTile();
        }
    }
    
    /**
     * Places the tiles on the diagonal of the board.
     * 
     */
    private void playDiagonal(){
        model.initializeDiagonalPositions();
        model.pickFourTiles();
        while(!model.getListFourTiles().isEmpty()){
            view.displayFourTiles();
            int index = view.askIndex(model.getListFourTiles().size());
            Position pos = view.askPosition();
            boolean isCorrectPos = model.checkDiagonalPosition(pos);
            while(!isCorrectPos){
                view.displayError("The position is not correct, "
                        + "try again.");
                pos = view.askPosition();
                isCorrectPos = model.checkDiagonalPosition(pos);
            }
            model.putDiagonal(index, pos);
        }
    }
}
