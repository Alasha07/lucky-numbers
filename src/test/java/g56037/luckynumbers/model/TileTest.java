/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g56037.luckynumbers.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Alan
 */
public class TileTest {
    
    public TileTest() {
    }
    
    @Test
    public void flipFaceUp_hidden_case(){
        Tile tile = new Tile(3);
        tile.flipFaceUp();
        assertTrue(tile.isFaceUp());
    }
    
    @Test
    public void flipFaceUp_not_hidden_case(){
        Tile tile = new Tile(3);
        tile.flipFaceUp();
        tile.flipFaceUp();
        assertTrue(tile.isFaceUp());
    }
    
    @Test
    public void flipFaceUp_not_hidden_case2(){
        Tile tile = new Tile(3);
        assertFalse(tile.isFaceUp());
    }
    
}
