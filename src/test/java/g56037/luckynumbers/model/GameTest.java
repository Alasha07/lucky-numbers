package g56037.luckynumbers.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import static g56037.luckynumbers.model.State.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MCD <mcodutti@he2b.be>
 */
public class GameTest {

    private Game game;

    @BeforeEach
    public void setUp() {
        game = new Game();
    }

    /* =====================
         Tests for start()
       ===================== */

    /* --- test related to the state --- */
    @Test
    public void start_when_game_not_started_ok() {
        game.start(4);
    }

    @Test
    public void start_when_game_over_ok() {
        fullPlay();
        game.start(2);
    }

    /* Play a game till the end */
    private void fullPlay() {
        game.start(2);
        int value = 1;
        int line = 0;
        int col = 0;
        for (int turn = 1; turn < game.getBoardSize() * game.getBoardSize(); turn++) {
            for (int player = 0; player < game.getPlayerCount(); player++) {
                game.pickTile(value);
                game.putTile(new Position(line, col));
                game.nextPlayer();
            }
            value++;
            col++;
            if (col == game.getBoardSize()) {
                col = 0;
                line++;
            }
        }
        game.pickTile(20);
        game.putTile(new Position(line,col));
    }

    @Test
    public void start_when_game_in_progress_ISE() {
        game.start(4);
        assertThrows(IllegalStateException.class,
                () -> game.start(1));
    }

    @Test
    public void start_state_changed_to_PICK_TILE() {
        game.start(3);
        assertEquals(State.PICK_TILE, game.getState());
    }

    /* --- tests related to the parameter --- */
    @Test
    public void start_playerCount_too_small_Exception() {
        assertThrows(IllegalArgumentException.class,
                () -> game.start(1));
    }

    @Test
    public void start_playerCount_minimum_accepted() {
        game.start(2);
    }

    @Test
    public void start_playerCount_maximum_accepted() {
        game.start(4);
    }

    @Test
    public void start_playerCount_too_big_Exception() {
        assertThrows(IllegalArgumentException.class,
                () -> game.start(5));
    }

    /* -- tests related to fields initialization --- */
    @Test
    public void start_playerCount_initialized() {
        game.start(4);
        assertEquals(4, game.getPlayerCount());
    }

    @Test
    public void start_current_player_is_player_0() {
        game.start(4);
        assertEquals(0, game.getCurrentPlayerNumber());
    }

    @Test
    public void getBoardSize_simple_test(){
        game.start(2);
        int size = game.getBoardSize();
        assertEquals(4,size);
    }
    
    @Test
    public void pickFaceDownTile_normal_case(){
        game.start(2);
        Tile tile = game.pickFaceDownTile(4);
        Tile expectedTile = new Tile(5);
        assertEquals(expectedTile.getValue(), tile.getValue());
    }
    
    @Test
    public void pickFaceDownTile_not_correct_state(){
        assertThrows(IllegalStateException.class, ()->game.pickFaceDownTile(7));
    }
    
    @Test
    public void pickFaceUpTile(){
        game.start(2);
        game.pickFaceDownTile(0);// player 1
        game.dropTile();
        game.nextPlayer();
        Tile tile = game.pickFaceDownTile(0); // player 2
        game.dropTile();
        game.nextPlayer();
        game.pickFaceDownTile(0); // player 1
        game.dropTile();
        game.nextPlayer(); // player 2
        // 1 2 3
        game.pickFaceUpTile(tile);
        assertEquals(2, tile.getValue());
    }
    
    @Test
    public void pickFaceUpTile_second_case(){
        game.start(2);
        game.pickFaceDownTile(0);// player 1
        game.dropTile();
        game.nextPlayer();
        
        Tile tile = game.pickFaceDownTile(0); // player 2
        game.dropTile();
        game.nextPlayer();
        
        Tile tile2 = game.pickFaceDownTile(0); // player 1
        game.dropTile();
        game.nextPlayer();
        
        game.pickFaceUpTile(tile);// player 2
        game.putTile(new Position(0,0));
        
        game.nextPlayer(); // player1
        // 1  3
        game.pickFaceUpTile(tile2);
        assertEquals(3, tile2.getValue());
    }
    
    @Test
    public void pickFaceUpTile_not_correct_case(){
        assertThrows(IllegalStateException.class, ()-> game.pickFaceUpTile(new Tile(3)));
    }
    
    @Test
    public void dropTile(){
        game.start(2);
        Tile pickedTile = game.pickFaceDownTile(0);
        game.dropTile();
        assertEquals(1, pickedTile.getValue());
    }
    
    @Test
    public void dropTile_not_started_state(){
        assertThrows(IllegalStateException.class, ()->game.dropTile());
    }
    
    @Test
    public void dropTile_palce_tile_state(){
        game.start(2);
        Tile pickedTile = game.pickFaceDownTile(0);
        game.dropTile();
        game.nextPlayer();
        game.pickFaceUpTile(pickedTile);
        assertThrows(IllegalStateException.class, ()->game.dropTile());
    }
    
    @Test
    public void faceDownTileCount_starting_game(){
        game.start(2);
        int count = game.faceDownTileCount();
        assertEquals(40,count);
    }
    
    @Test
    public void faceDownTileCount_mid_game(){
        game.start(2);
        int midFaceDownTiles = 19;
        for(int i = 0 ; i <= midFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.dropTile();
            game.nextPlayer();
        }
        int count = game.faceDownTileCount();
        assertEquals(20,count);
    }
    
    @Test
    public void faceDownTileCount_no_face_down_tiles(){
        game.start(2);
        int zeroFaceDownTiles = 39;
        int lastTile = 39;
        for(int i = 0 ; i <= zeroFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.dropTile();
            if(i != lastTile)
                game.nextPlayer();
        }
        int count = game.faceDownTileCount();
        assertEquals(0,count);
    }
    
    @Test
    public void faceDownTileCount_state_not_started(){
        assertThrows(IllegalStateException.class, ()-> game.faceDownTileCount());
    }
    
    @Test
    public void faceUpTileCount_starting_game(){
        game.start(2);
        int count = game.faceUpTileCount();
        assertEquals(0,count);
    }
    
    @Test
    public void faceUpTileCount_mid_game(){
        game.start(2);
        int midFaceDownTiles = 19;
        for(int i = 0 ; i <= midFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.dropTile();
            game.nextPlayer();
        }
        int count = game.faceUpTileCount();
        assertEquals(20,count);
    }
    
    @Test
    public void faceUpTileCount_end_game(){
        game.start(2);
        int zeroFaceDownTiles = 39;
        int lastTile = 39;
        for(int i = 0 ; i <= zeroFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.dropTile();
            if (i != lastTile)
                game.nextPlayer();
        }
        int count = game.faceUpTileCount();
        assertEquals(40,count);
    }
    
    @Test
    public void faceUpTileCount_state_not_started(){
        assertThrows(IllegalStateException.class, ()-> game.faceUpTileCount());
    }
    
    @Test
    public void getAllFaceUpTiles_not_started_state(){
        assertThrows(IllegalStateException.class, ()-> game.getAllFaceUpTiles());
    }
    
    @Test
    public void getAllFaceUpTiles_no_face_up_tiles(){
        game.start(2);
        List<Tile> list = game.getAllFaceUpTiles();
        assertEquals(0, list.size());
    }
    
    @Test
    public void getAllFaceUpTiles_several_face_up_tiles(){
        game.start(2);
        int midFaceDownTiles = 19;
        for(int i = 0 ; i <= midFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.dropTile();
            game.nextPlayer();
        }
        List<Tile> list = game.getAllFaceUpTiles();
        assertEquals(20, list.size());
    }
    
    @Test
    public void putTile_when_position_not_correct(){
        game.start(2);
        game.pickFaceDownTile(18);
        //game.putTile(new Position(9,4));
        assertThrows(IllegalArgumentException.class, 
                () -> game.putTile(new Position(4,4)));
    }
    
    @Test
    public void putTile_when_state_not_place_tile(){
        game.start(2);
        assertThrows(IllegalStateException.class, 
                () -> game.putTile(new Position(2,2)));
    }
    
    @Test
    public void putTile_tile_face_down(){
        game.start(2);
        game.pickFaceDownTile(6);
        game.putTile(new Position(0,2));
    }
    
    @Test
    public void putTile_tile_face_up(){
        game.start(2);
        game.pickFaceDownTile(6);
        game.dropTile();
        game.nextPlayer();
        game.pickFaceDownTile();
        game.putTile(new Position(0,2));
    }
    
    @Test
    public void nextPlayer_not_correct_state(){
        game.start(2);
        assertThrows(IllegalStateException.class, 
                () -> game.nextPlayer());
    }
    
    @Test
    public void nextPlayer_normal_case(){
        game.start(2);
        game.pickFaceDownTile(15);
        game.putTile(new Position(0,0));
        game.nextPlayer();
        int player = game.getCurrentPlayerNumber();
        assertEquals(1, player);
    }
    
    @Test
    public void nextPlayer_drop_tile(){
        game.start(2);
        game.pickFaceDownTile(15);
        game.dropTile();
        game.nextPlayer();
        int player = game.getCurrentPlayerNumber();
        assertEquals(1, player);
    }
    
    @Test
    public void getPlayerCount_not_correct_state(){
        assertThrows(IllegalStateException.class, 
                () -> game.getPlayerCount());
    }
    
    @Test
    public void getPlayerCount_maximum_case(){
        game.start(4);
        assertEquals(4,game.getPlayerCount());
    }
    
    @Test
    public void getPlayerCount_minimum_case(){
        game.start(2);
        assertEquals(2,game.getPlayerCount());
    }
    
    @Test
    public void getPlayerCount_intermediate_case(){
        game.start(3);
        assertEquals(3,game.getPlayerCount());
    }
    
    @Test
    public void getState_not_started_case(){
        State state = game.getState();
        assertEquals(NOT_STARTED, state);
    }
    
    @Test
    public void getState_pick_tile_case(){
        game.start(2);
        State state = game.getState();
        assertEquals(PICK_TILE, state);
    }
    
    @Test
    public void getState_place_or_drop_tile_case(){
        game.start(2);
        game.pickFaceDownTile(6);
        State state = game.getState();
        assertEquals(PLACE_OR_DROP_TILE, state);
    }
    
    @Test
    public void getState_place_tile_case(){
        game.start(2);
        Tile tile = game.pickFaceDownTile(6);
        game.dropTile();
        game.nextPlayer();
        game.pickFaceUpTile(tile);
        State state = game.getState();
        assertEquals(PLACE_TILE, state);
    }
    
    @Test
    public void getState_turn_end_case(){
        game.start(2);
        game.pickFaceDownTile(6);
        game.putTile(new Position(0,0));
        State state = game.getState();
        assertEquals(TURN_END, state);
    }
    @Test
    public void getState_turn_end_to_pick_tile_case(){
        game.start(2);
        game.pickFaceDownTile(6);
        game.putTile(new Position(0,0));
        game.nextPlayer();
        State state = game.getState();
        assertEquals(PICK_TILE, state);
    }
    
    @Test
    public void getCurrentPlayerNumber_when_state_is_not_started(){
        Model game = new Game();
        assertThrows(IllegalStateException.class, 
                () -> game.getCurrentPlayerNumber());
    }
    
    @Test
    public void getCurrentPlayerNumber_normal_case(){
        game.start(4);
        int currentPlayer = game.getCurrentPlayerNumber();
        assertEquals(0,currentPlayer);
    }
    
    @Test
    public void getPickedTile_not_place_tile_state(){
        assertThrows(IllegalStateException.class, 
                () -> game.getPickedTile());
    }
    
    @Test
    public void getPickedTile_minimum_case(){
        game.start(2);
        game.pickFaceDownTile(0);
        Tile tile = game.getPickedTile();
        Tile expectedTile = new Tile(1);
        assertEquals(tile.getValue(), expectedTile.getValue());
    }
    
    @Test
    public void getPickedTile_maximum_case(){
        game.start(2);
        /*1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
          1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
        */
        game.pickFaceDownTile(19);
        Tile tile = game.getPickedTile();
        Tile expectedTile = new Tile(20);
        assertEquals(tile.getValue(), expectedTile.getValue());
    }
    
    @Test
    public void getPickedTile_normal_case(){
        game.start(2);
        game.pickFaceDownTile(10);
        Tile tile = game.getPickedTile();
        Tile expectedTile = new Tile(11);
        assertEquals(tile.getValue(), expectedTile.getValue());
    }
    
    @Test
    public void isInside_position_not_inside(){
        game.start(2);
        assertFalse(game.isInside(new Position(4,4)));
    }
    
    @Test
    public void isInside_position_inside(){
        game.start(2);
        assertTrue(game.isInside(new Position(2,1)));
    }
    
    @Test
    public void canTileBePut_position_not_in_the_board(){
        game.start(2);
        game.pickFaceDownTile(2);
        assertThrows(IllegalArgumentException.class, 
                () -> game.canTileBePut(new Position(4,4)));
    }
    
    @Test
    public void canTileBePut_state_not_place_tile(){
        assertThrows(IllegalStateException.class, 
                () -> game.canTileBePut(new Position(2,2)));
    }
    
    @Test
    public void canTileBePut_normal_case(){
        game.start(2);
        game.pickFaceDownTile(2);
        assertTrue(game.canTileBePut(new Position(0,1)));
    }
    
    @Test
    public void getTile_when_number_of_player_not_valid(){
        game.start(2);
        assertThrows(IllegalArgumentException.class, 
                () -> game.getTile(4, new Position(0,0)));
    }
    
    @Test
    public void getTile_when_number_of_position_not_valid(){
        game.start(2);
        assertThrows(IllegalArgumentException.class, 
                () -> game.getTile(1, new Position(4,4)));
    }
    
    @Test
    public void getTile_when_state_is_not_started(){
        assertThrows(IllegalStateException.class, 
                () -> game.getTile(1, new Position(2,1)));
    }
    
    @Test
    public void getTile_normal_case_first_player(){
        game.start(2);
        game.pickFaceDownTile(3);
        game.putTile(new Position(0,0));
        Tile tile = game.getTile(0, new Position(0,0));
        Tile expectedResult = new Tile(4);
        assertEquals(expectedResult.getValue(), tile.getValue());
    }
    
    @Test
    public void getTile_normal_case_second_player(){
        /*1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
          1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
        */
        game.start(2);
        game.pickFaceDownTile(4);
        /*1 2 3 4  6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
          1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
        */
        game.putTile(new Position(0,0));
        game.nextPlayer();
        game.pickFaceDownTile(7);
        /*1 2 3 4  6 7 8  10 11 12 13 14 15 16 17 18 19 20
          1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
        */
        game.putTile(new Position(2,1));
        Tile tile = game.getTile(1, new Position(2,1));
        Tile expectedResult = new Tile(9);
        assertEquals(expectedResult.getValue(), tile.getValue());
    }
    
    @Test
    public void getWinner_when_state_is_not_game_over(){
        assertThrows(IllegalStateException.class,
                () -> game.getWinners());
    }
    
    @Test
    public void getWinner_when_player_has_won(){
        game.start(2);
        int tile = 1;
        for(int i = 0 ; i < game.getPlayerCount() ; i++){
            game.initializeDiagonalPositions();
            game.pickFourTiles(tile);
            int row = 0;
            int column = 0;
            while(row < 4 && column < 4){
                game.putDiagonal(0, new Position (row,column));
                row++;
                column++;
            }
            game.nextPlayer();
        }
        //32 tuiles faces cachées
        game.pickTile(20);
        game.putTile(new Position(3,3));
        game.nextPlayer();
        game.pickTile(20);
        game.putTile(new Position(3,3));
        game.nextPlayer();
        game.pickTile(19);
        game.putTile(new Position(2,3));
        game.nextPlayer();
        
        //32 tuiles faces cachées
        while(game.faceDownTileCount() != 0){
            game.pickFaceDownTile();
            game.dropTile();
            if(game.faceDownTileCount() != 0)
                game.nextPlayer();
        }
        
        List <Integer> winner = new ArrayList <>();
        winner.add(0);
        assertEquals(winner,game.getWinners());
    }
    
    @Test
    public void getWinner_no_more_face_up_tiles(){
        game.start(2);
        int totalFaceDownTiles = 40;
        for (int i = 1 ; i <= totalFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.dropTile();
            if (i != totalFaceDownTiles)
                game.nextPlayer();
        }
        List <Integer> winners = new ArrayList <>();
        winners.add(0);
        winners.add(1);
        assertEquals(winners, game.getWinners());
    }
    
    @Test
    public void getWinner_several_winners(){
        game.start(4);
        
        int totalFaceDownTiles = 80;
        int index = 0;
        int row = 0;
        int column = 0;
        int cpt = 0;
        
        for(int i = 1 ; i <= totalFaceDownTiles ; i++){
            int player = game.getCurrentPlayerNumber();
            
            if((player == 1 || player == 3) && cpt < 10){
                game.pickFaceDownTile(index);
                game.putTile(new Position (row, column));
                column++;
                if(column == 4){
                    column = 0;
                    row++;
                }
                cpt++;
            }else{
                game.pickFaceDownTile(index);
                game.dropTile();
            }
            
            if (i != totalFaceDownTiles){
                game.nextPlayer();
            }
        }
        
        List <Integer> winners = new ArrayList <>();
        winners.add(1);
        winners.add(3);
        assertEquals(winners, game.getWinners());
    }
    
    @Test
    public void getWinners_all_players_won(){
        game.start(4);
        int totalFaceDownTiles = 80;
        
        for(int i = 1 ; i <= totalFaceDownTiles ; i++){
            game.pickFaceDownTile();
            game.putTile(new Position(3,3));
            if (i != totalFaceDownTiles){
                game.nextPlayer();
            }
        }
        
        List <Integer> winners = new ArrayList <>();
        winners.add(0);
        winners.add(1);
        winners.add(2);
        winners.add(3);
        assertEquals(winners, game.getWinners());
    }
    
    @Test
    public void getWinners_3_players_1_winner(){
        game.start(3);
        int totalTiles = 60;
        
        for (int i = 1 ; i <= totalTiles ; i++){
            int player = game.getCurrentPlayerNumber();
            if(player == 1){
                game.pickFaceDownTile();
                game.putTile(new Position(1,1));
            }else{
                game.pickFaceDownTile();
                game.dropTile();
            }
            if (i != totalTiles){
                game.nextPlayer();
            }
        }
        
        List <Integer> winner = new ArrayList <>();
        winner.add(1);
        assertEquals(winner, game.getWinners());
    }
    
    @Test 
    public void putDiagonal_normal_case(){
        game.start(2);
        for(int i = 0 ; i < 4 ; i++){
            game.pickFourTiles(i);
            //1 2 3 4
        }
        game.putDiagonal(0, new Position(1,1));
        //2 3 4
        List <Tile> fourTiles = game.getListFourTiles();
        Tile result = fourTiles.get(2); //4
        Tile expectedResult = new Tile(4);
        assertEquals(expectedResult.getValue(), result.getValue());
    }
    
    @Test 
    public void putDiagonal_last_index(){
        game.start(2);
        for(int i = 0 ; i < 4 ; i++){
            game.pickFourTiles(i);
            //1 2 3 4
        }
        game.putDiagonal(3, new Position(1,1));
        //1 2 3 
        List <Tile> fourTiles = game.getListFourTiles();
        Tile result = fourTiles.get(2); //3
        Tile expectedResult = new Tile(3);
        assertEquals(expectedResult.getValue(), result.getValue());
    }
    
    @Test 
    public void putDiagonal_several_tiles_removed(){
        game.start(2);
        for(int i = 0 ; i < 4 ; i++){
            game.pickFourTiles(i);
            //1 2 3 4
        }
        for(int j = 0 ; j < 2 ; j++){
            game.putDiagonal(j, new Position(1,1));
        }
        // 2 4
        List <Tile> fourTiles = game.getListFourTiles();
        Tile result = fourTiles.get(0); 
        Tile expectedResult = new Tile(2);
        assertEquals(expectedResult.getValue(), result.getValue());
    }
    
    @Test 
    public void putDiagonal_not_valid_index(){
        game.start(2);
        assertThrows(IndexOutOfBoundsException.class, ()->game.putDiagonal(4, 
                new Position(1,1)));
    }
    
    @Test 
    public void checkDiagonalPosition_true_case(){
        game.start(2);
        game.initializeDiagonalPositions();
        boolean result = game.checkDiagonalPosition(new Position(1,1));
        assertTrue(result);
    }
    
    @Test 
    public void checkDiagonalPosition_false_case(){
        game.start(2);
        boolean result = game.checkDiagonalPosition(new Position(4,4));
        assertFalse(result);
    }
    @Test 
    public void checkDiagonalPosition_false_case2(){
        game.start(2);
        boolean result = game.checkDiagonalPosition(new Position(1,3));
        assertFalse(result);
    }
    
    @Test 
    public void checkDiagonalPosition_minimum_position(){
        game.start(2);
        game.initializeDiagonalPositions();
        boolean result = game.checkDiagonalPosition(new Position(0,0));
        assertTrue(result);
    }
    
    @Test 
    public void checkDiagonalPosition_maximum_position(){
        game.start(2);
        game.initializeDiagonalPositions();
        boolean result = game.checkDiagonalPosition(new Position(3,3));
        assertTrue(result);
    }
    
    @Test 
    public void checkDiagonalPosition_several_positions_removed_correctPos(){
        game.start(2);
        game.initializeDiagonalPositions();
        game.checkDiagonalPosition(new Position(0,0));
        game.checkDiagonalPosition(new Position(3,3));
        boolean result = game.checkDiagonalPosition(new Position(2,2));
        assertTrue(result);
    }
    
    @Test 
    public void checkDiagonalPosition_several_positions_removed_notCorrectPos(){
        game.start(2);
        Position pos = new Position(3,3);
        game.checkDiagonalPosition(new Position(0,0));
        game.checkDiagonalPosition(pos);
        boolean result = game.checkDiagonalPosition(pos);
        assertFalse(result);
    }
    
    @Test 
    public void checkDiagonalPosition_no_more_positions(){
        game.start(2);
        Position pos = new Position(2,2);
        game.checkDiagonalPosition(new Position(0,0));
        game.checkDiagonalPosition(new Position(1,1));
        game.checkDiagonalPosition(pos);
        game.checkDiagonalPosition(new Position(3,3));
        boolean result = game.checkDiagonalPosition(pos);
        assertFalse(result);
    }
    
    @Test 
    public void checkDiagonalPosition_no_more_positions2(){
        game.start(2);
        Position pos = new Position(0,0);
        game.checkDiagonalPosition(pos);
        game.checkDiagonalPosition(new Position(1,1));
        game.checkDiagonalPosition(new Position(2,2));
        game.checkDiagonalPosition(new Position(3,3));
        boolean result = game.checkDiagonalPosition(pos);
        assertFalse(result);
    }
}
