/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g56037.luckynumbers.model;

import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author Alan
 */
public class DeckTest {
    
    private Deck deck;
    
    @BeforeEach
    public void setUp(){
        deck = new Deck(2);
    }

    @Test
    public void faceDownCount_2_players(){
        Deck myDeck = new Deck(2);
        int count = myDeck.faceDownCount();
        assertEquals(40,count);
    }
    
    @Test
    public void faceDownCount_3_players(){
        Deck myDeck = new Deck(3);
        int count = myDeck.faceDownCount();
        assertEquals(60,count);
    }
    
    @Test
    public void faceDownCount_4_players(){
        Deck myDeck = new Deck(4);
        int count = myDeck.faceDownCount();
        assertEquals(80,count);
    }
    
    @Test
    public void pickFaceDown_normal_case(){
        Tile pickedTile = deck.pickFaceDown(10);
        assertEquals(new Tile(11).getValue(), pickedTile.getValue());
    }
    
    @Test
    public void pickFaceDown_minimal_tile(){
        Tile pickedTile = deck.pickFaceDown(0);
        assertEquals(new Tile(1).getValue(), pickedTile.getValue());
    }
    
    @Test
    public void pickFaceDown_maximal_tile(){
        Tile pickedTile = deck.pickFaceDown(39);
        assertEquals(new Tile(20).getValue(), pickedTile.getValue());
    }
    
    @Test
    public void faceUpCount_no_tiles_visible(){
        int nb = deck.faceUpCount();
        assertEquals(0,nb);
    }
    
    @Test
    public void faceUpCount_one_tile_visible(){
        deck.putBack(new Tile(1));
        int nb = deck.faceUpCount();
        assertEquals(1,nb);
    }
    
    @Test
    public void faceUpCount_all_tiles_visible(){
        for(int i = 1 ; i <= deck.faceDownCount() ; i++){
            int j = 1;
            deck.putBack(new Tile(j));
            j++;
            if(j == 21){
                j = j/21;
            }
        }
        int nb = deck.faceUpCount();
        assertEquals(40,nb);
    }
    
    @Test
    public void hasFaceUp_not_exists(){
        boolean result = deck.hasFaceUp(new Tile(2));
        assertFalse(result);
    }
    
    @Test
    public void hasFaceUp_exists(){
        Tile tile = new Tile(7);
        tile.flipFaceUp();
        deck.putBack(tile);
        boolean result = deck.hasFaceUp(tile);
        assertTrue(result);
    }
    
    @Test
    public void hasFaceUp_wrong_number_tile(){
        Tile tile = new Tile(0);
        assertThrows(IllegalArgumentException.class, ()-> deck.hasFaceUp(tile));
    }
    
    @Test
    public void hasFaceUp_wrong_number_tile_other_case(){
        Tile tile = new Tile(21);
        assertThrows(IllegalArgumentException.class, ()-> deck.hasFaceUp(tile));
    }
    
    @Test
    public void pickFaceUp(){
        Tile tile = new Tile(2);
        deck.putBack(tile);
        tile.flipFaceUp();
        deck.pickFaceUp(tile);
        assertEquals(new Tile (2).getValue(),tile.getValue());
    }
    
    @Test
    public void pickFaceUp_0_face_up_tiles(){
        Tile tile = new Tile(2);
        assertThrows(IllegalArgumentException.class, ()-> deck.pickFaceUp(tile));
    }
    
    @Test
    public void pickFaceUp_wrong_tile(){
        Tile tile = new Tile(2);
        deck.putBack(new Tile(17));
        assertThrows(IllegalArgumentException.class, ()-> deck.pickFaceUp(tile));
    }
    
    @Test
    public void putBack(){
        deck.putBack(new Tile(11));
        deck.putBack(new Tile(7));
        List <Tile> visibleTiles = deck.getAllFaceUp();
        Tile lastTile = visibleTiles.get(1);
        Tile expectedResult = new Tile(7);
        assertEquals(expectedResult.getValue(), lastTile.getValue());
    }
}
