# Lucky Numbers V2

## Divers

- Javadoc :  en ordre
- régularité des commits : en ordre

## Analyse de code

Commence par jeter un œil aux remarques générées par Intelliji dans la page index.html

Reviens-vers moi si il y a des remarques que tu ne comprends pas ou que tu ne trouves pas appropriées

## model

### Tile

- `faceUp` : quel est l'intérêt de vérifier que la face est visible avant de la modifier dans ce cas ?
- tests unitaires : en ordre

#### Deck

- Lors de la suppression d'une tuile d'une liste, les lignes 

- ```
  Tile pickedTile = faceDownTiles.get(index);
  faceDownTiles.remove(index);
  ```

  peuvent être remplacées par 

  ```
  Tile pickedTile = faceDownTiles.remove(index);
  ```

- constructeur : en ordre

- `faceDownCount` : en ordre

- `pickFaceDown` : en ordre

- `faceUpCount` : en ordre

- `getAllFaceUp` : en ordre

- `hasFaceUp` : l'utilisation de la méthode `contains` de  `List` simplifierai le code

- `pickFaceUp` : en ordre

- `putBack` : en ordre

- tests unitaires : pas de test pour vérifier le lancement de l'exception si le paramètre est  `null` pour la méthode `hasFaceUp`

#### Game

- Interface `model` : mot clé `public` inutile dans cette interface
- `pickFaceDownTile` : en ordre
- `pickFaceUpTile` : en ordre
- `dropTile` : en ordre
- `faceDownTileCount` : en ordre
- `faceUpTileCount` : en ordre
- `getAllfaceUpTiles` : en ordre
- mise à jour des méthodes existantes : en ordre

## view

- nombre de tuiles restantes face cachée  : en ordre
- détail des tuiles visibles : en ordre
- choix de pioche : en ordre
- conseil : `answer.equalsIgnoreCase("b")` se réécrit `"b".equalsIgnoreCase(answer)`, de cette façon si `answer` est `null`, aucun `NullPointerException` n'est lancé

## controller

en ordre

## Fin du jeu

- nouvelle règle (plus de tuiles) : en ordre
- `getWinners` : les deux boucles de `defineWinners` pourraient fusionner
- affichage : en ordre



## Début du jeu

- `README` : en ordre
- implémentation : 
  - que se passe-t-il si la méthode `putDIagonal` de `Game` reçoit une position qui n'est pas sur la diagonale (exemple (0,1)) ? la méthode est-elle cohérente ou y-a-t-il une obligation d'appeler `checkDiagonalPosition` avant ?
  - 

## Construction du jar

en ordre

