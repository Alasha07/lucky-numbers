# Lucky Numbers V1

## Divers

- Javadoc :  en ordre
- régularité des commits : en ordre

## Analyse de code

Commence par jeter un œil aux remarques générées par Intelliji dans la page index.html

Reviens-vers moi si il y a des remarques que tu ne comprends pas ou que tu ne trouves pas appropriées

## model

### Tile

- en ordre

#### Position

- en ordre

#### Board

- attribut : en ordre
- constructeur : en ordre
- `getSize` : en ordre
- `isInside` : en ordre
- `getTile` : en ordre
- `canBePut` : en ordre
- certaines lignes de codes se répètent dans les méthodes `check`, as-tu une idée de comment factoriser le code de ces méthodes ?
- `put`: en ordre
- `isFull` : en ordre
- en anglais `colonne` devient `column` et `ligne` devient `row`

#### State

- en ordre

#### Game

- `canTileBePut_normal_case` n'a pas son annotation `Test`
- j'ajouterai un test pour vérifier que la méthode `putTile` ne m'autorise pas à ajouter une tuile alors que c'est interdit (ce test me permet d'être certains que la méthode `board.canTileBePut()` est appelée par `game.putTile`)

## view

- on peut ajouter dans le constructeur la vérification des paramètres via
  - `this.model = Objects.requireNonNull(model,"Model not found");`
  - c'est utile pour pourvoir rendre le vue indépendant et ainsi la réutiliser
- quand tu utilises `i ` dans des boucles quand tu en as l'occasion essaies de remplacer `i` par un nom plus explicite `row` ou `column` par exemple, ça rend le code plus lisible

## controller

- les `System.out.println` doivent se situer dans la classe `MyView` et non dans le contrôleur, ça facilite la maintenance du logiciel (changement de langue du logiciel par exemple) 
- dans la méthode `playAgain` on pourrait remplacer les appels à `String readString()` par des appels à `boolean askConfirm()`, ceci permettrait au contrôleur d'être indépendant de la réponse attendue, un `y` dans ce cas, mais ça pourrait être un `o` si la vue est en français 
- `correctPositions` et `correctPlayerCount` utilise des constantes pour savoir si ces méthodes ont le droit d'accepter l'entrée de l’utilisateur. Mais l'information doit venir de la partie du programme qui connaît les règles du jeu (les classes du package `model`). Par exemple le fait que la position est dans un tableau de 4x4 est gérée par la méthode `canTileBePut`, il est inutile vu les conditions que tu as mises de tester individuellement via les chiffres 0 et 3 si les positions sont valides. Si tu souhaites tout de même vérifier les valeurs des lignes et des colonnes par ces chiifres (0 et 3) ils doivent être défini en constante dans une classe du package `model` afin de centraliser les mises à jours. Dans ton code si je décide d'autoriser des plateaux de 7x7 une mise à jour doit être effectuée dans la méthode `isInside` et dans le `Controller`. Cette double mise à jour est risque d'oubli et donc de bug. C'est pour ça qu'on parle de bonnes pratiques. 